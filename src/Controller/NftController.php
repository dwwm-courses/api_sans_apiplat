<?php

namespace App\Controller;

use App\Entity\Nft;
use App\Form\NftType;
use App\Repository\NftRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class NftController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer){
       $this->serializer = $serializer;
    }

    #[Route('/nfts', name: 'app_nft', methods: ["GET"])]
    public function index(NftRepository $nftRepository): JsonResponse
    {
        $nfts = $nftRepository->findAll();
        $serializedObject =
            json_decode($this->serializer->serialize($nfts, 'json'));

        return $this->json($serializedObject);
    }

    #[Route('/nfts/{nft}', name:'nft_one', methods: ["GET"])]
    public function getOne(Nft $nft){
        $serializedObject = json_decode(
            $this->serializer->serialize($nft, 'json'));

        return $this->json($serializedObject);
    }

    #[Route('/nfts/{nft}', name:'nft_delete', methods: ["DELETE"])]
    public function deleteOne(Nft $nft, EntityManagerInterface $em){
        $em->remove($nft);
        $em->flush();

        $response = new Response();
        $response->setStatusCode(204);
        return $response;
    }

    #[Route('/nfts', name: 'app_nft_create', methods: ["POST"])]
    public function add(Request $request, EntityManagerInterface $em)
    {
        $objectRequest = json_decode($request->getContent(), true);

        $form = $this->createForm(NftType::class);
        $form->submit($objectRequest);

        if ($form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            $retour = json_decode($this->serializer->serialize($form->getData(), "json"));

            $response = $this->json($retour);
            $response->setStatusCode(201);
            return $response;

        } else {

            $response = $this->json([
                "success" => false,
                "message" => $form->getErrors(true)
            ]);
            $response->setStatusCode(400);

            return $response;
        }
    }

        #[Route('/nfts/{nft}', name: 'app_nft_update', methods: ["PUT"])]
        public function update(Nft $nft, Request $request, EntityManagerInterface $em){
            $objectRequest = json_decode($request->getContent(), true);

            $form = $this->createForm(NftType::class, $nft);

            $form->submit($objectRequest);


            if($form->isValid()){
                $em->flush();
                $nft = $form->getData();
                $nftSerialized =
                    json_decode($this->serializer->serialize($nft, "json"));

                return new JsonResponse($nftSerialized);
            } else{
                $response = $this->json([
                    "success" => false,
                    "errors" => $form->getErrors(true)
                ]);

                $response->setStatusCode(400);

                return $response;
            }
        }


}
